from django.db import models
from django.contrib.auth.models import User

from django.urls import reverse

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(User, related_name="projects")

    def __str__(self) -> str:
        return self.name

    def get_absolute_url(self):
        return reverse("show_project", kwargs={"pk": self.pk})
